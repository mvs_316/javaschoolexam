package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {

    private static final Map<String, Integer> operatorPriority= new HashMap<>();

    private boolean comparePrioryty(String op1, String op2) {
        return (operatorPriority.containsKey(op1) &&
                operatorPriority.get(op1) >= operatorPriority.get(op2));
    }


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            Queue<String> rPn = convertToRPN(statement);
            Double result = evaluateRPN(rPn);

            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
            DecimalFormat decimalFormat = new DecimalFormat("#.####", otherSymbols);
            return decimalFormat.format(result);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts statement from infix notation to reverse Polish notation
     */
    private Queue<String> convertToRPN(String statement) {

        operatorPriority.put("+", 1);
        operatorPriority.put("-", 1);
        operatorPriority.put("*", 2);
        operatorPriority.put("/", 2);

        Stack<String> operators = new Stack<>();
        Queue<String> output = new LinkedList<>();

        String[] split = statement.split("(?<=[-+*/()])|(?=[-+*/()])");
        int i = 0;
        while (i < split.length) {
            String token = split[i];
            if (operatorPriority.containsKey(token)) {
                while (!operators.isEmpty() &&
                        comparePrioryty(operators.peek(), token))
                    output.offer(operators.pop());
                operators.push(token);
            } else if (token.equals("("))
                operators.push(token);

            else if (token.equals(")")) {
                if (operators.peek() != null)
                    while (!operators.peek().equals("("))
                        output.offer(operators.pop());
                operators.pop();
            } else output.offer(token);
            i++;
        }

        while (!operators.isEmpty()) {
            output.offer(operators.pop());
        }
        return output;
    }


    private Double evaluateRPN(Queue<String> entry) {

        Stack<Double> stack = new Stack<>();

        Iterator<String> iterator = entry.iterator();
        while (iterator.hasNext()) {
            String token = iterator.next();
            switch (token) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    stack.push(-stack.pop() + stack.pop());
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    double divisor = stack.pop();
                    if (divisor == 0) {
                        throw new ArithmeticException("/ by zero");
                    }
                    stack.push(stack.pop() / divisor);
                    break;
                default:
                    stack.push(Double.parseDouble(token));
            }
        }

        return stack.pop();
    }

}
