package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {

            int sideSize = getSideSize(inputNumbers.size());
            Collections.sort(inputNumbers);
            int colNum = (2 * sideSize) - 1;
            int middle = (colNum - 1) / 2;

            int[][] result = new int[sideSize][colNum];
            boolean setNumber = true;
            Iterator<Integer> it = inputNumbers.iterator();

            int i = 0;
            while (i < sideSize) {
                int j = middle;
                while (j < colNum - middle) {
                    if (setNumber) {
                        result[i][j] = it.next();
                    }
                    setNumber = !setNumber;
                    j++;
                }
                setNumber = !setNumber;
                i++;
                middle--;
            }

            return result;
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }
    private int getSideSize(int num) throws IllegalArgumentException {
        if (num < 0) {
            throw new IllegalArgumentException("Number must be triangular");
        }

        double triangularSqrt = (Math.sqrt(8 * num + 1) - 1) / 2;

        if (triangularSqrt % 1 != 0) {
            throw new IllegalArgumentException("Number must be triangular");
        }

        return (int) triangularSqrt;
    }


}
